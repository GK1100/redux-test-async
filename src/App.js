import './App.css';
import React from "react";
import RandomDataList from "./components/randomDataList";

function App() {
  return (
    <div className="App">
      <RandomDataList />
    </div>
  );
}

export default App;
