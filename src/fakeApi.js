const generateFakeData = () => {
    const items = [];
    for (let i = 0; i < Math.floor(Math.random() * 5 + 5); i++) {
        items.push(Math.floor(Math.random() * 1000))
    }
    return items;
}

export const getFakeRandomData = (timeout) => {
    return new Promise(resolve => setTimeout(() => resolve(generateFakeData()), timeout));
};
