import * as types from "./actionTypes";

export const requestNewData = (items) => ({type: types.REQUEST_NEW_DATA, items});

export const cancelDataRequest = () => ({type: types.CANCEL_REQUEST});
