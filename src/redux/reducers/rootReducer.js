import * as types from "../actions/actionTypes";

const initialState = {items: []}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.REQUEST_NEW_DATA:
            return Object.assign({}, state, { items: action.items });
        default:
            return state;
    }
};

export default rootReducer;
