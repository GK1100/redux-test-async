import {getFakeRandomData} from "../../fakeApi";
import {requestNewData} from "../actions/actions";

export const getNewData = (timeout) => (dispatch) => {
    getFakeRandomData(timeout).then(res => {
        dispatch(requestNewData(res));
    });
}