import React from "react";
import {connect} from "react-redux";
import {getNewData} from "../redux/creators/creators";

const RandomDataList = (props) => {
    const items = [];
    for (const [index, value] of props.items.entries()) {
        items.push(<li key={index}>{value}</li>);
    }
    return (
        <div>
            <ul>{items}</ul>
            <button onClick={props.refresh}>Refresh</button>
        </div>
    );
}

const mapStateToProps = (state) => ({
    items: state.items
});

const mapDispatchToProps = (dispatch) => ({
    refresh: () => dispatch(getNewData(3000)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RandomDataList);
